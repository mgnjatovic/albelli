##Prerequisites##
Vagrant 2 (https://www.vagrantup.com/downloads.html)  
VirtualBox 5.1 (https://www.virtualbox.org/wiki/Download_Old_Builds_5_1)

##Setup##
In the root folder run vagrant up to run the box.  
When installation is finished app is accessible on 192.168.33.44.  
The project is installed in /var/www/html/albelli.

##Contact##
gnjatovic.marija@gmail.com