<?php

class Model {
    function __construct() {
        try {
            $this->posts = DATA_SOURCE . "blogposts.csv";
            $this->words = DATA_SOURCE . "words.csv";
        } catch (Exception $e) {
            exit('Something is wrong with the data source file');
        }
    }

    /**
     * Get all posts
     */
    public function getAllPosts() {
        $posts = self::get($this->posts);
        if (count($posts) > 0)
            $posts = array_reverse($this->parsePosts($posts));
        return $posts;
    }

    public function getTopWords() {
        $words = $this->getWords();
        if (count($words) > 0)
            $words = array_slice($words, 0, 5, true);
        return $words;
    }

    /**
     * Add a post
     */
    public function addPost($post, $files) {
        $post['title'] = filter_input(INPUT_POST, "title");
        $post['content'] = filter_input(INPUT_POST, "content");
        $post['email'] = filter_input(INPUT_POST, "email");
        $post['image'] = $files['image']['name'];
        $post['timestamp'] = date("Y-m-d H:i");
        require APP . 'view/posts/post.php';
        $this->save($post, $this->posts);
        $this->updateWords($post['content']);
    }

    /**
     * @param $posts
     * @return $posts
     */
    private function parsePosts($posts) {
        foreach ($posts as &$post) {
            $post['title'] = $post[0];
            $post['content'] = htmlspecialchars($post[1]);
            $post['email'] = $post[2];
            $post['image'] = 'resized_' . $post[3];
            $post['timestamp'] = $post[4];
            unset($post[0]);
            unset($post[1]);
            unset($post[2]);
            unset($post[3]);
            unset($post[4]);
        }
        return $posts;
    }

    private function updateWords($content) {
        preg_match_all("/[a-z0-9']{4,}/i", $content, $words);
        $wordscount = array_count_values($words[0]);
        $existingWords = $this->getWords();

        //delete file before writing next
        if (count($existingWords) > 0)
            unlink($this->words);

        foreach ($wordscount as $key => $value) {
            if (array_key_exists($key, $existingWords))
                $existingWords[$key] += $wordscount[$key];
            else
                $existingWords[$key] = $wordscount[$key];
        }
        arsort($existingWords);
        $this->save($existingWords, $this->words);
    }

    private function getWords() {
        $existingWords = array();
        $words = self::get($this->words);
        if ($words) {
            $existingWords = array_combine($words[0], $words[1]);
            arsort($existingWords);
        }
        return $existingWords;
    }

    /**
     * @param $data
     */
    private function save($data, $filename) {
        if (file_exists($filename)) {
            $fp = fopen($filename, "a");
        } else {
            $fp = fopen($filename, "w");
        }
        if (filesize($filename) == 0) {
            fputcsv($fp, array_keys($data));
        }
        fputcsv($fp, $data);
        fclose($fp);
        ob_start();
        ob_get_clean();
    }

    /**
     * @param $filename
     * @return array
     */
    private
    function get($filename) {
        if (file_exists($filename)) {
            if (($handle = fopen($filename, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
                    $items[] = $data;
                }
                fclose($handle);
            }
            return $items;
        }
    }
}
