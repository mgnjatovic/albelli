<div class="row">
    <div class="panel">
        <div class="small-10 medium-10 large-10 columns">
            <h1>
                <?= $post['title']; ?>
            </h1>
        </div>
        <div class="small-2 medium-2 large-2 columns">
            <small>
                <?= $post['timestamp']; ?>
            </small>
        </div>
        <div class="row">
            <?php
            if (isset($post['image']) && $post['image']) {
                ?>
                <div class="small-6 medium-6 large-6 columns">
                    <img width="300" height="300" src="<?= 'img/' . $post['image']?>">
                </div>
                <?php
            }
            ?>
            <?php
            if (isset($post['content'])) {
                ?>
                <div class="small-6 medium-6 large-6 columns">
                    <p><?= $post['content'] ?></p>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
