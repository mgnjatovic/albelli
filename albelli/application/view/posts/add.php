<fieldset>
    <div id="loaderInput" align="center"><img src="img/loading.gif"><br><br><br></div>
    <form id="post_form" name="post_form" method="post" enctype="multipart/form-data">
        <div class="small-12 medium-12 large-12 columns" id="blog_form_title">
            <h1>New blog post</h1>
        </div>
        <div class="small-12 medium-12 large-12 columns" id="status_message">
        </div>
        <div class="small-12 medium-12 large-9 columns">
            <div class="large-12 small-12 columns">
                <input type="text" name="title" id="title" placeholder="My post title" required="required">
            </div>
            <div class="large-12 small-12 columns">
                <textarea name="content" id="content" placeholder="Here all my important post text!"></textarea>
            </div>
            <div class="large-12 small-12 columns">
                <input type="email" name="email" id="email" placeholder="Email Address" required="required">
            </div>
            <div class="large-12 small-12 columns">
                <div class="large-6 small-6 columns">
                    <input type="file" id="image" name="image">
                </div>
                <div class="large-6 small-6 columns">
                    <button id="save_image" title="save" class="button extra-small">
                        Upload image
                    </button>
                </div>

            </div>
        </div>
        <div class="small-12 medium-12 large-3 columns">
            <button id="save_post" title="save" class="button small" type="submit"> Save
                post
            </button>
        </div>
    </form>
</fieldset>