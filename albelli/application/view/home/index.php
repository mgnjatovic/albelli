<html lang="en">
<head>
    <meta charset="utf-8">
    <title>My spectacular blog</title>
    <link rel="stylesheet" href="//cdn-files.cloud/arc/css/arc.discovery.min.css"/>
    <!--[if IE 8]>
    <link rel="stylesheet" href="//cdn-files.cloud/arc/css/arc-ie8fix.min.css"/><![endif]-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <!--jQuery Validation 1.16.0-->
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script type='text/javascript' src="js/blog.js"></script>
</head>
<body>
<div class="row">
    <div class="small-12 medium-12 large-12 columns text-center">
        <h1>My spectacular blog</h1>
        <h2>A totally false statement</h2>
    </div>
</div>
<div class="row">
    <div class="small-12 medium-12 large-12 columns">
        <?php require APP .'view/posts/add.php'; ?>
    </div>
</div>
<div class="row">

    <div class="small-12 medium-12 large-12 columns">
        <div class="small-12 medium-12 large-9 columns" id="post_list"><div id="loaderPosts" align="center"><img src="img/loading.gif"></div></div>
        <div class="small-12 medium-12 large-3 columns" id="word_list"></div>
    </div>
</div>
</body>
</html>
