<?php

class Home extends Controller {
    /**
     * PAGE: index
     */
    public function index() {
        // load views
        require APP . 'view/home/index.php';
    }
}
