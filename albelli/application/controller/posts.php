<?php

/**
 * Class Posts
 */
class Posts extends Controller {
    /**
     * ACTION: index
     * Retrieves all posts from data source
     */
    public function index() {
        $posts = $this->model->getAllPosts();
        sleep(3);
        if (count($posts)) {
            //remove header
            array_pop($posts);
            foreach ($posts as $post)
                require APP . 'view/posts/post.php';
        }
    }

    /**
     * ACTION: words
     * Get top used words in posts
     */
    public function words() {
        $words = $this->model->getTopWords();
        if (count($words)) {
            foreach ($words as $word => $count)
                echo '<p>' . $word . '</p>';
        }
    }

    /**
     * ACTION: addPost
     * Add new post to blog
     */
    public function add() {
        $valid = $this->validate();
        sleep(3);

        if ($valid)
            $this->model->addPost($_POST, $_FILES);
        else
            header('HTTP/1.1 500 Error');
    }

    /**
     * ACTION: addImage
     * Adding image on button click
     */
    public function image() {
        $file = current($_FILES);
        if ($this->validateImage()) {
            $fn = $file['tmp_name'];
            $size = getimagesize($fn);
            $ratio = $size[0] / $size[1]; // width/height
            if ($ratio > 1) {
                $width = 300;
                $height = 300 / $ratio;
            } else {
                $width = 300 * $ratio;
                $height = 300;
            }
            $src = imagecreatefromstring(file_get_contents($fn));
            $dst = imagecreatetruecolor($width, $height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
            imagedestroy($src);
            imagepng($dst, ROOT . "public/img/resized_" . $file['name']); // adjust format as needed
            imagedestroy($dst);
        } else {
            header('HTTP/1.1 500 Invalid image type');
        }
    }

    /**
     * Validating blog per parameters from the task
     */
    private function validate() {
        $isBlogOwner = $_POST['email'] == BLOG_OWNER;
        $isValidFEToken = $_SERVER['HTTP_AUTHORITY'] == AUTH;
        $valid = $this->validateImage() && $isBlogOwner && $isValidFEToken;
        return $valid;
    }

    private function validateImage(){
        $ext = pathinfo(current($_FILES)['name'], PATHINFO_EXTENSION);
        return in_array($ext, array('jpg', 'png'));
    }
}
