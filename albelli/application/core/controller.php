<?php

class Controller {
    /**
     * @var null Model
     */
    public $model = null;

    /**
     * Whenever controller is created load "the model".
     */
    function __construct() {
        $this->loadModel();
    }

    /**
     * Loads the "model".
     * @return object model
     */
    public function loadModel() {
        require APP . 'model/model.php';
        $this->model = new Model();
    }
}
