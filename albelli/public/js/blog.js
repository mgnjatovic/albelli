$(document).ready(function () {

    var IsLoaderForInput = false;
    $("#loaderInput").hide();

    $(document).ajaxStart(function () {
        if (IsLoaderForInput) {
            $("#loaderInput").show();
            $("#post_form").hide();
        }
        else {
            $("#loaderPosts").show();
        }
    }).ajaxComplete(function () {
        if (IsLoaderForInput) {
            $("#loaderInput").hide();
            $("#post_form").show();
        }
        else {
            $("#loaderPosts").hide();
        }
    });
    //Get all posts
    $.ajax({
        type: 'get',
        url: 'posts/index',
        processData: false,
        contentType: false,
        success: function (responseData) {
            $("#post_list").prepend((responseData) ? responseData : "<div class=\"row\"><div class=\"panel\">No posts</div></div>");
        },
        error: function () {
            $("#post_list").append("<div class=\"row\"><div class=\"panel\">Error reading posts</div></div>");
        }
    });

    //Get top words
    $.ajax({
        type: 'get',
        url: 'posts/words',
        processData: false,
        contentType: false,
        success: function (responseData) {
            console.log(responseData);
            $("#word_list").append((responseData) ? "<div class=\"panel\">" + responseData + "</div>" : "<div class=\"panel\">No words</div>");
        },
        error: function () {
            $("#word_list").append("<div class=\"panel\">noWords</div>");
        }
    });

    // Save image file
    $(document).on('change', '#image', function () {
        $("#save_image").on("click", function (e) {
            e.preventDefault();
            var file = document.getElementById('image').files[0];
            var form_data = new FormData();
            form_data.append(file.name, file);
            $.ajax({
                type: 'post',
                url: 'posts/image',
                data: form_data,
                processData: false,
                cache: false,
                contentType: false,
                success: function (data) {
                    $("#status_message").append("<div class='alert-box success'>Succesfully saved image<a href='' class='close'>×</a></div>");
                    $('#status_message').fadeOut(1000);
                },
                error: function (responseData) {
                    $('#status_message').fadeIn(1000);
                    $("#status_message").append("<div class='alert-box error'>Error uploading image<a href='' class='close'>×</a></div>");
                    $('#status_message').fadeOut(1000);
                }
            });
        });
    });

    //Validate and save form, for some reason conditional validation is not working as it should
    $('#post_form').validate({
        rules: {
            title: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            content: {
                required: function (element) {
                    return $("#image").is(":empty");
                },
            },
            image: {
                required: function (element) {
                    return $("#content").is(":empty");
                }
            }
        },
        submitHandler: function (form) {
            IsLoaderForInput = true;
            var form_data = new FormData($("#post_form")[0]);
            $.ajax({
                type: 'post',
                url: 'posts/add',
                data: form_data,
                beforeSend: function (request) {
                    request.setRequestHeader("Authority", "UfJXGQlNQNaZkxfjaqyCkPYUAYOdiJOE");
                },
                processData: false,
                contentType: false,
                success: function (responseData) {
                    $("#save_post").addClass("disabled");
                    $("#post_form")[0].reset();
                    $("#save_post").removeClass("disabled");
                    $("#status_message").append("<div class='alert-box success'>Succesfully saved<a href='' class='close'>×</a></div>");
                    $('#status_message').fadeOut(1000);
                    $("#post_list").prepend(responseData);
                },
                error: function (responseData) {
                    $('#status_message').fadeIn(1000);
                    $("#status_message").append("<div class='alert-box error'>Error<a href='' class='close'>×</a></div>");
                    $('#status_message').fadeOut(1000);
                }
            });

            return false;
        }
    });
});